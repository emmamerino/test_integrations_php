<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../test_integrations_php/assets/css/bootstrap.min.css">
  <title>Detalle Posts</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <span class="navbar-brand" href="#">Detalle Posts</span>
        <form class="d-flex">
          <a href="#" onClick="window.history.go(-1)" class="btn btn-sm btn-light" data-bs-toggle="tooltip" data-bs-dismiss="click" data-bs-custom-class="tooltip-inverse" data-kt-initialized="1">Regresar</a>
        </form>
      </div>
    </div>
  </nav>

    <?php 

        $id = $_POST['id'];
        $posts = "https://jsonplaceholder.typicode.com/posts/$id/comments";
        $jsonPosts = file_get_contents($posts);
        $arrayPosts = json_decode($jsonPosts, true);
      
    ?>
    <br><br>
    <div class="container-sm">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Comentario</th>
          </tr>
        </thead>
        <?php 
          foreach ($arrayPosts as $key => $comentario):
        ?>
        <tbody>
          <tr>
            <th scope="row"><?= $cadena = $comentario["id"]; ?></th>
            <td><?= $cadena = $comentario["name"]; ?></td>
            <td><?= $cadena = $comentario["email"]; ?></td>
            <td><?= $cadena = $comentario["body"]; ?></td>
          </tr>
        </tbody>
         <?php 
          endforeach;
        ?>
      </table>
    </div>
</body>
</html>