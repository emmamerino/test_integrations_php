<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../test_integrations_php/assets/css/bootstrap.min.css">
	<title>Listado de Personas</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <div class="container-fluid">
	    <span class="navbar-brand" href="#">Listado de Personas</span>
	      
	    </div>
	  </div>
	</nav>

	<?php

		$url = "https://jsonplaceholder.typicode.com/users";
		$json = file_get_contents($url);
		$arreglo = json_decode($json, true);

	?>
	<br>
	<div class="container-md">
		<table class="table table-sm table-striped">
		  	<thead>
			    <tr>
			      <th scope="col"></th>
			      <th scope="col">#</th>
			      <th scope="col">Nombre Usuario</th>
			    </tr>
		  	</thead>
		  <?php 
		  	foreach ($arreglo as $key => $nombre):
		  ?>
		  <tbody>
		    <tr class="accordion-toggle" data-bs-toggle="collapse" href="#collapse<?= $nombre["id"]; ?>" data-target="#collapse<?= $nombre["id"]; ?>">
		    	<td scope="row"><button class="btn btn-secondary btn-sm">+</button></td>
		    	<th scope="row"><?= $nombre["id"]; ?></th>
			    <td scope="row" data-bs-toggle="collapse" href="#collapse<?= $nombre["id"]; ?>"><?= $cadena = $nombre["name"]; ?></th>
		      		<div id="collapse<?= $nombre["id"]; ?>" class="collapse in" data-bs-parent="#accordion<?= $nombre["id"]; ?>" aria-labelledby="#accordion<?= $nombre["id"]; ?>">
					    <div class="card-body">
					    	<table class="table table-borderless">
								<thead>
								    <tr>
								      <th scope="col">Usuario</th>
								      <th scope="col">Email</th>
								      <th scope="col">Telefono</th>
								      <th scope="col">Web Site</th>
								      <th scope="col">Posts</th>
								      <th scope="col">Todos</th>
								    </tr>
							  	</thead>
							  	<tbody>
							    <tr>
							      <td><?= $cadena = $nombre["username"]; ?></td>
							      <td><?= $cadena = $nombre["email"]; ?></td>
							      <td><?= $cadena = $nombre["phone"]; ?></td>
							      <td><?= $cadena = $nombre["website"]; ?></td>
							      <td>
							      	
							      	<form action="posts.php" method="post">
							      		<input type="hidden" name="id" value="<?= $nombre["id"]; ?>">
							      		<button type="submit" class="btn btn-secondary btn-sm">Posts</button>
							      	</form>	
							      		
							      </td>
							      <td>
							      	
						      		<form action="todos.php" method="post">
						      			<input type="hidden" name="id" value="<?= $nombre["id"]; ?>">
							      		<button type="submit" class="btn btn-secondary btn-sm">Todos</button>	
							      	</form>
							      		
							      </td>
							    </tr>
							  	</tbody>
							</table>
					        
					    </div>
				    </div>
		    	</tr>
		   </tbody>
		  <?php 
		  	endforeach;
		  ?>
		</table>
	</div>
	<script type="text/javascript" src="../test_integrations_php/assets/js/bootstrap.min.js"></script>
</body>
</html>