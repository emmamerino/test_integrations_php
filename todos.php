<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../test_integrations_php/assets/css/bootstrap.min.css">
  <title>Detalle Posts</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <span class="navbar-brand" href="#">Detalle Todos</span>
        <form class="d-flex">
          <a href="#" onClick="window.history.go(-1)" class="btn btn-sm btn-light" data-bs-toggle="tooltip" data-bs-dismiss="click" data-bs-custom-class="tooltip-inverse" data-kt-initialized="1">Regresar</a>
        </form>
      </div>
    </div>
  </nav>

    <?php 

        $id = $_POST['id'];
        $todos = "http://jsonplaceholder.typicode.com/users/$id/todos";
        $jsonTodos = file_get_contents($todos);
        $arrayTodos = json_decode($jsonTodos, true);
     
    ?>
    <br><br>
    <div class="container-sm">
      <div class="form-row mb-3 justify-content-end mr-0">
          <div class="float-right">
            <div class="d-grid gap-2 d-md-block">
              <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Agregar registro
              </button> 
              <!-- Modal -->
              <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Nueva Tarea</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" action="registro.php">
                      <div class="modal-body">
                          <div class="mb-3">
                            <label for="titulo" class="form-label">Titulo</label>
                            <input type="text" name="title" class="form-control" id="title" aria-describedby="title">
                          </div>
                          <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="completed" name="completed" value="1">
                            <label class="form-check-label" for="exampleCheck1">Completed</label>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <br>

      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Titulo</th>
            <th scope="col">Detalle</th>
          </tr>
        </thead>
        <?php 
          foreach ($arrayTodos as $key => $tarea):
        ?>
        <tbody>
          <tr>
            <th scope="row"><?= $cadena = $tarea["id"]; ?></th>
            <td><?= $cadena = $tarea["title"]; ?></td>
            <td><?= $cadena = $tarea["completed"]; ?></td>
          </tr>
        </tbody>
         <?php 
          endforeach;
        ?>
      </table>
    </div>
    <script type="text/javascript" src="../test_integrations_php/assets/js/bootstrap.min.js"></script>
</body>
</html>